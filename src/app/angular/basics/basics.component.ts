import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-basics',
  templateUrl: './basics.component.html',
  styleUrls: ['./basics.component.scss']
})
export class BasicsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToAngularPage() {
    this.router.navigateByUrl("");
  }

}
