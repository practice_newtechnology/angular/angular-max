import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalFileStyleComponent } from './external-file-style.component';

describe('ExternalFileStyleComponent', () => {
  let component: ExternalFileStyleComponent;
  let fixture: ComponentFixture<ExternalFileStyleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalFileStyleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalFileStyleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
