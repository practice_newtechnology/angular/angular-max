import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectByClassComponent } from './select-by-class.component';

describe('SelectByClassComponent', () => {
  let component: SelectByClassComponent;
  let fixture: ComponentFixture<SelectByClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectByClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectByClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
