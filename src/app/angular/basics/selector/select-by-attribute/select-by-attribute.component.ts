import { Component, OnInit } from '@angular/core';

@Component({
  selector: '[app-select-by-attribute]', // Angular select the element by attribute
  templateUrl: './select-by-attribute.component.html',
  styleUrls: ['./select-by-attribute.component.scss']
})
export class SelectByAttributeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
