import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectByAttributeComponent } from './select-by-attribute.component';

describe('SelectByAttributeComponent', () => {
  let component: SelectByAttributeComponent;
  let fixture: ComponentFixture<SelectByAttributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectByAttributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectByAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
