import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectByElementComponent } from './select-by-element.component';

describe('SelectByElementComponent', () => {
  let component: SelectByElementComponent;
  let fixture: ComponentFixture<SelectByElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectByElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectByElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
