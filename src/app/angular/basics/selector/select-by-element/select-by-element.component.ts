import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-select-by-element', // Angular select the element by element itself
  templateUrl: './select-by-element.component.html',
  styleUrls: ['./select-by-element.component.scss']
})
export class SelectByElementComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
