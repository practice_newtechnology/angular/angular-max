import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalFileTemplateComponent } from './external-file-template.component';

describe('ExternalFileTemplateComponent', () => {
  let component: ExternalFileTemplateComponent;
  let fixture: ComponentFixture<ExternalFileTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalFileTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalFileTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
