import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-external-file-template',
  templateUrl: './external-file-template.component.html', // Template as external file for this component
  styleUrls: ['./external-file-template.component.scss']
})
export class ExternalFileTemplateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
