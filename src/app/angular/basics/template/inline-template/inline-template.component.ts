import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inline-template',
  template: `<p> Inline template of component </p>`,
  styleUrls: ['./inline-template.component.scss']
})
export class InlineTemplateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
