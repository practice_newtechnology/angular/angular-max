import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  studentForm: FormGroup;
  genders = [
    { value: 'male', view: 'Male'},
    { value: 'female', view: 'Female'},
    { value: 'other', view: 'Other'}
  ];
  forbiddenUsernames = ['Yuvi', 'Ranjith'];
  forbiddenEmails = ['yuvi@gmail.com', 'ranjith@gmail.com'];

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.studentForm = new FormGroup({
      studentId: new FormGroup({
        username: new FormControl(null, [Validators.required, this.isUsernameExist.bind(this)]),  // Adding custom validators
        email: new FormControl(null, [Validators.required, Validators.email], this.isEmailExist.bind(this))  // Adding multiple validators and async validators
      }),
      name: new FormControl(null, Validators.required), // Adding single Validators
      gender: new FormControl('male'),
      educationDetails: new FormArray([])
    });
    /**
     *  1. Formcontrol - 1st argument was default value, 2nd argument was sync validators and 3rd argument was async validators
     *  2. During async operation - ng-pending class was added to element(between ng-valid and ng-invalid state)
     *  3. this.studentForm.valueChanges.subscribe(value => {
            console.log(value);
          });
          this.studentForm.statusChanges.subscribe(status => {
            console.log(status);
          });
     */
  }

  addEducationDetails() {
    const formControl = new FormControl('', Validators.required);
    this.educationDetails.push(formControl);
  }

  isUsernameExist(control: FormControl): { [key: string]: boolean } {
    if(!this.forbiddenUsernames.indexOf(control.value)) {
      return { 'usernameExist': true };
    }
    return null;
  }

  isEmailExist(control: FormControl): Observable<{ [key: string]: boolean }> | Promise<{ [key: string]: boolean }> {
    const promise = new Promise<{ [key: string]: boolean }>((resolve, reject) => {
      // setTimeout(() => {
        console.log(control.value);
        if(this.forbiddenEmails.indexOf(control.value) !== -1) {
          return resolve({ 'emailExist': true });
        } else {
          return resolve(null);
        }
      // }, 1000);

    });
    return promise;
  }

  submitStudentForm() {
    console.log(this.studentForm);
  }

  resetStudentForm() {
    this.studentForm.reset();
  }

  patchValue() {
    this.studentForm.patchValue({
      studentId: {
        email: 'logesh@gmail.com'
      }
    })
  }

  setValue() {
    this.studentForm.setValue({
      studentId: {
        username: 'logi',
        email: 'logesh@gmail.com'
      },
      name: 'logesh',
      gender: 'male',
      educationDetails: []
    });
  }

  get username() { return this.studentForm.get('studentId.username') as FormControl }
  get email() { return this.studentForm.get('studentId.email') as FormControl }
  get educationDetails() { return this.studentForm.get('educationDetails') as FormArray }

  goToAngularPage() {
    this.router.navigateByUrl("");
  }
}
