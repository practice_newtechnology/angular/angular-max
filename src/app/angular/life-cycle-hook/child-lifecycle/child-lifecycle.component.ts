import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, ContentChild, DoCheck, ElementRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-child-lifecycle',
  templateUrl: './child-lifecycle.component.html',
  styleUrls: ['./child-lifecycle.component.scss']
})
export class ChildLifecycleComponent implements OnChanges, OnInit, DoCheck, 
  AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {
  childVar = "Logesh View";
  
  @Input() name: string;
  @Input() age: number;
  @ViewChild('localReferenceByViewChild', { static: true }) viewChildEleRef : ElementRef;
  @ContentChild('parentReferenceByContentChild', { static: true }) contentChildEleRef: ElementRef;


  constructor() { 
    console.log("constructor is called");
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    console.log("ngOnChanges is called");
    console.log(simpleChanges);
  }

  ngOnInit() {
    console.log("ngOnInit is called");
    console.log("-> View Child Value is ", this.viewChildEleRef.nativeElement.textContent);
    console.log("-> Content Child Value is ", this.contentChildEleRef.nativeElement.textContent);
  }

  ngDoCheck() { // Fired when angular check for changes
    console.log("ngDoCheck is called");
  }

  ngAfterContentInit() {
    console.log("ngAfterContentInit is called");
    console.log("-> Content Child Value is ", this.contentChildEleRef.nativeElement.textContent);
  }

  ngAfterContentChecked() {  // Fired after ngDoCheck()
    console.log("ngAfterContentChecked is called");
  }

  ngAfterViewInit() {
    console.log("ngAfterViewInit is called");
    console.log("-> View Child Value is ", this.viewChildEleRef.nativeElement.textContent);
  }

  ngAfterViewChecked() {
    console.log("ngAfterViewChecked is called");
  }

  ngOnDestroy() {
    console.log("ngOnDestroy is called");
  }

  eventOccur() {
    console.log("button clicked");
  }

}
