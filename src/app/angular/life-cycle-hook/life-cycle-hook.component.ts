import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-life-cycle-hook',
  templateUrl: './life-cycle-hook.component.html',
  styleUrls: ['./life-cycle-hook.component.scss']
})
export class LifeCycleHookComponent implements OnInit {
  name = "Logesh";
  age = 20;
  parentVar = "Logesh Content";

  constructor(private router: Router) { }

  ngOnInit(): void {
    setTimeout(() => this.age = 30, 10000);
  }

  goToAngularPage() {
    this.router.navigateByUrl("");
  }

}
