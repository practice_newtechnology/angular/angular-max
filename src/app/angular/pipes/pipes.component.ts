import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.scss']
})
export class PipesComponent implements OnInit {
  students = [
    { name: 'Logesh Waran', doj: new Date(), gender: 'male' },
    { name: 'Padmavathi', doj: new Date(), gender: 'female' }
  ];

  logesh;
  ranjith;
  filterValue = '';

  nickName = new Promise((resolve, reject) => {
    setTimeout(()=> resolve('Logi'), 3000);
  })

  constructor(private router: Router) { }
 
  ngOnInit(): void {
    this.logesh = this.students[0];
    this.ranjith = this.students[1];
  }

  addStudent() {
    this.students.push({ name: 'Ranjith Kumar', doj: new Date(), gender: 'male' });
  }

  goToAngularPage() {
    this.router.navigateByUrl("");
  }

}
