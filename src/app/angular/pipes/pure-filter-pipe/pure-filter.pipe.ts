import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pureFilter'
})
export class PureFilterPipe implements PipeTransform {

  transform(value: any, propName: string, filterValue: string): any {
    /**
     * 1. It won't executed if arrya and object value changes
     * 2. Executed only when the data changes
     */
    console.log('Pure -> ', value, filterValue);
    if(value.length === 0 || filterValue === '') {
      return value;
    } else {
      const result = [];
      for(const item of value) {
        if(item[propName] === filterValue) {
          result.push(item);
        }
      }
      return result;  
    }
  }

}
