import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'impureFilter',
  pure: false
})
export class ImpureFilterPipe implements PipeTransform {

  transform(value: any, propName: string, filterValue: string): unknown {
    console.log('Impure -> ', value, filterValue);
    if(value.length === 0 || filterValue === '') {
      return value;
    } else {
      const result = [];
      for(const item of value) {
        if(item[propName] === filterValue) {
          result.push(item);
        }
      }
      return result;  
    }
  }

}
