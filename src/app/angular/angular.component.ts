import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-angular',
  templateUrl: './angular.component.html',
  styleUrls: ['./angular.component.scss']
})
export class AngularComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToBasicsPage() {
    this.router.navigate(['/basics']);
  }
  
  goToDatabindingPage() {
    this.router.navigate(['/databinding']);
  }

  goToDirectivesPage() {
    this.router.navigate(['/directives']);
  }

  goToComponentCommunicationPage() {
    this.router.navigate(['/componentCommunication']);
  }

  goToLifecycleHookPage() {
    this.router.navigate(['/lifeCycleHook']);
  }

  goToFormPage() {
    this.router.navigate(['/form']);
  }

  goToPipesPage() {
    this.router.navigate(['/pipes']);
  }

  goToHttpPage() {
    this.router.navigate(['/http']);
  }

}
