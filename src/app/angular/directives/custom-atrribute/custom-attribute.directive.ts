import { Directive, ElementRef, HostBinding, HostListener, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appCustomAttribute]'
})
export class CustomAttributeDirective implements OnInit {
  @HostBinding('style.color') color = "white";

  constructor(private renderer: Renderer2, private elementRef: ElementRef) { }

  ngOnInit() {
    // Accessing the element directly is not good practice
    // this.elementRef.nativeElement.style.backgroundColor = 'blue';
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'green');
  }

  @HostListener('mouseenter') mouseover(event: Event) {
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'yellow');
    this.color = "black";
  }

  @HostListener('mouseleave') mouseleave(event: Event) {
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'purple');
    this.color = "orange";
  }

}
