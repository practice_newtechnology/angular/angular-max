import { Directive, ElementRef, HostBinding, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appCustomAttributeDatabinding]'
})
export class CustomAttributeDatabindingDirective {
  @Input() defaultColor = 'green';
  @Input() highlightColor = 'yellow';
  @HostBinding('style.background') backgroundColor;

  constructor(private renderer: Renderer2, private elementRef: ElementRef) { }

  ngOnInit() {
    this.backgroundColor = this.defaultColor;
  }

  @HostListener('mouseenter') mouseover(event: Event) {
    this.backgroundColor = this.highlightColor;
  }

  @HostListener('mouseleave') mouseleave(event: Event) {
    this.backgroundColor = this.defaultColor;
  }

}
