import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.scss']
})
export class DirectivesComponent implements OnInit {
  num = 19;
  nums = [1, 2, 3];
  bool = true;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  getBackgroundColor() {
    return this.num % 2 == 0 ? '#18a558': '#ff5c5c';
  }

  goToAngularPage() {
    this.router.navigateByUrl("");
  }

}
