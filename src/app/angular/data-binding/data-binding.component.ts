import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.scss']
})
export class DataBindingComponent implements OnInit {
  property = "Hello";
  disablePropertyButton = true;
  clickEventTriggered = 0;
  inputEventValue = "";
  disableProperytAndEventButton = true;
  studentName = "karthick";
  inputTemplateVal = "";
  @ViewChild('localReferenceByViewChild', { static: true }) viewChildEleRef : ElementRef;
  viewChildVal = "";

  constructor(private router: Router) { 
    setTimeout(() => {
      this.disablePropertyButton = false;
    }, 5000);
  }

  ngOnInit(): void {
  }

  getStudentName() {
    return this.property;
  }

  clickEvent() {
    this.clickEventTriggered++;
  }

  setInputEventValue(event: Event) {
    this.inputEventValue = (<HTMLInputElement> event.target).value;
  }

  changePropertyAndEventButtonDisableProperty() {
    this.disableProperytAndEventButton = !this.disableProperytAndEventButton;
  }

  setStudentName(event: Event) {
    this.studentName = (<HTMLInputElement> event.target).value;
  }

  setInputValue(localReferenceOfInputTemplate) {
    this.inputTemplateVal = (<HTMLInputElement>localReferenceOfInputTemplate).value;
  }

  setViewChildInputValue() {
    // this.viewChildEleRef.nativeElement.value = "Illegal";  Don't access the DOM directly
    this.viewChildVal = this.viewChildEleRef.nativeElement.value;
  }

  goToAngularPage() {
    this.router.navigateByUrl("");
  }

}
