import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtwCompComponent } from './btw-comp.component';

describe('BtwCompComponent', () => {
  let component: BtwCompComponent;
  let fixture: ComponentFixture<BtwCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtwCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtwCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
