import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {
  @Input() childVar;
  @Output() childEmitVal = new EventEmitter<String>();

  constructor() { }

  ngOnInit(): void {
  }

  passValToParent() {
    this.childEmitVal.emit("sangi");
  }

}
