import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit {
  parentVal = "logi";
  parentVar;

  constructor() { }

  ngOnInit(): void {
  }

  setChildValToParentVar(childEmitVal) {
    this.parentVar = childEmitVal;
  }

}
