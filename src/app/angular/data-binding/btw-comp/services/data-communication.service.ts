import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataCommunicationService {
  childVal = "child val";
  parentVal = "parent val";

  constructor() { }

  setParentVal(parentVal) {
    this.parentVal = parentVal;
  }

  setChildVal(childVal) {
    this.childVal = childVal;
  }

  logData() {
    console.log(this.parentVal, this.childVal);
  }

}
