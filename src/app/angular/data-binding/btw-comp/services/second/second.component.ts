import { Component, OnInit } from '@angular/core';
import { CentralizedCodeService } from '../centralized-code.service';
import { DataCommunicationService } from '../data-communication.service';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.scss']
})
export class SecondComponent implements OnInit {
  childVar : string;
  childVal : string = "sangi service";

  constructor(private centralizedCodeService: CentralizedCodeService, private dataCommunication: DataCommunicationService) { }

  ngOnInit(): void {
  }

  commonMethod() {
    this.centralizedCodeService.commonMethodOfComponent("Second Component");
  }

  logData() {
    this.dataCommunication.logData();
  }

  setChildVal() {
    this.dataCommunication.setChildVal(this.childVal);
  }

}
