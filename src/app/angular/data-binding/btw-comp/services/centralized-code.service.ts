import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CentralizedCodeService {

  constructor() { }

  commonMethodOfComponent(value: string) {
    console.log(value , " -> Centralized the code if two or more components uses the same method");
  }
}
