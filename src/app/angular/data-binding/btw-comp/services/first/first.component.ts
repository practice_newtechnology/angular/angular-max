import { Component, OnInit } from '@angular/core';
import { CentralizedCodeService } from './../centralized-code.service';
import { DataCommunicationService } from './../data-communication.service'


@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.scss']
})
export class FirstComponent implements OnInit {
  parentVal = "logi service";
  parentVar;

  constructor(private centralizedCodeService: CentralizedCodeService, private dataCommunication: DataCommunicationService) { }

  ngOnInit(): void {
  }

  commonMethod() {
    this.centralizedCodeService.commonMethodOfComponent("First Component");
  }

  logData() {
    this.dataCommunication.logData();
  }

  setParentVal() {
    this.dataCommunication.setParentVal(this.parentVal);
  }

}
