import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-btw-comp',
  templateUrl: './btw-comp.component.html',
  styleUrls: ['./btw-comp.component.scss']
})
export class BtwCompComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToAngularPage() {
    this.router.navigateByUrl("");
  }

}
