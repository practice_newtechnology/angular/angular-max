import { Injectable } from '@angular/core';
import { Student } from './student.model';
import { HttpClient, HttpEventType, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  baseUrl = "https://angular-concept-d9f44-default-rtdb.firebaseio.com/"

  constructor(private http: HttpClient) { }

  getStudents() {
    // let newParams = new HttpParams();
    // newParams = newParams.append('print', 'pretty');
    // newParams = newParams.append('key2', 'value2');

    return this.http
      .get<{ [key: string]: Student }>(
        // this.baseUrl + "students.json?print=pretty&key2=value2"
        this.baseUrl + "students.json",
        {
          headers: new HttpHeaders({ 'Custom-Header': 'Logi'}),
          params: new HttpParams().set('print', 'pretty')
          // params: newParams;
        }
      )
      .pipe(
        map((responseData) => {
          const transformData: Student[] = [];
          for(const key in responseData) {
            if(responseData.hasOwnProperty(key)) {
              transformData.push({ ...responseData[key], id: key });
            }
          }
          return transformData;
        }),
        catchError((errorResponse) => {
          // Error handling task -> Send analytics to server
          return throwError(errorResponse);
        })
      );
  }

  postStudent(postStudentData: Student) {
    /**
     * 1. If component is not required the response, you can subscribe in service itself
     */
    this.http
    .post<{ name: string }>(
      this.baseUrl + "students.json/",
      postStudentData,
      {
        observe: 'response'
        // observe: 'body' // default value is body if we not specified
      }
    )
    .subscribe((response) => {
      console.log(response);
    });
  }

  deleteStudents() {
    return this.http
      .delete(
        this.baseUrl + "students.json",
        {
          observe: 'events',
          responseType: 'json'
          // responseType: 'text'  // check response.body = null if json, = "null" if text
        }
      )
      .pipe(
        tap(event => {
          if(event.type === HttpEventType.Sent) {
            console.log(event);
          } else if(event.type === HttpEventType.Response) {
            console.log(event);
          }
        })
      );
  }
}
