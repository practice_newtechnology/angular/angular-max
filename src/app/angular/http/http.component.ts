import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { HttpService } from './http.service'
import { Student } from './student.model'

@Component({
  selector: 'app-http',
  templateUrl: './http.component.html',
  styleUrls: ['./http.component.scss']
})
export class HttpComponent implements OnInit {
  students = [];
  isLoading = true;
  error = null;
  studentForm: FormGroup;
  genders = [
    { value: 'male', view: 'Male'},
    { value: 'female', view: 'Female'},
    { value: 'other', view: 'Other'}
  ];

  constructor(private router: Router, private httpService: HttpService) { }

  ngOnInit(): void {
    this.studentForm = new FormGroup({
      name: new FormControl(null),
      gender: new FormControl('male'),
    });

    this.getStudents();
  }

  submitStudentForm() {
    this.postStudent(this.studentForm.value);
  }

  errorReacted() {
    this.error = null;
  }

  getStudents() {
    this.httpService.getStudents().subscribe(
      (students) => {
        console.log(students);
        this.students = students;
        this.isLoading = false;
      },
      (error) => {
        console.log(error);
        this.error = error.message;
        this.isLoading = false;
      }
    );
  }

  postStudent(postStudentData: Student) {
    this.httpService.postStudent(postStudentData);
  }

  deleteStudents() {
    this.httpService.deleteStudents().subscribe((response) => {
      this.students = [];
    })
  }

  goToAngularPage() {
    this.router.navigateByUrl("");
  }
}
