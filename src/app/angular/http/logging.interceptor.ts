import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpEventType
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('Request is on the way');
    return next.handle(request)
    .pipe(
      tap(event => {
        if(event.type === HttpEventType.Response) {
          console.log('Response arrived, body data');
          console.log(event.body);
        }
      })
    )
  }
}
