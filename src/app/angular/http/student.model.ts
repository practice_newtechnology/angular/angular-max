export interface Student {
    name: string;
    gender: string;
    id?: string
}