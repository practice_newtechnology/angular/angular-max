import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AngularComponent } from './angular/angular.component';
import { BasicsComponent } from './angular/basics/basics.component';
import { DataBindingComponent } from './angular/data-binding/data-binding.component';
import { DirectivesComponent } from './angular/directives/directives.component';
import { BtwCompComponent } from './angular/data-binding/btw-comp/btw-comp.component';
import { LifeCycleHookComponent } from './angular/life-cycle-hook/life-cycle-hook.component';
import { FormComponent } from './angular/form/form.component';
import { PipesComponent } from './angular/pipes/pipes.component';
import { HttpComponent } from './angular/http/http.component';

const routes: Routes = [
  { path: '', component: AngularComponent }, // once the path was reached, component is loaded on the outlet - see augury
  { path: 'basics', component: BasicsComponent },
  { path: 'databinding', component: DataBindingComponent },
  { path: 'directives', component: DirectivesComponent },
  { path: 'componentCommunication', component:  BtwCompComponent},
  { path: 'lifeCycleHook', component:  LifeCycleHookComponent },
  { path: 'form', component:  FormComponent },
  { path: 'pipes', component: PipesComponent},
  { path: 'http', component: HttpComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
