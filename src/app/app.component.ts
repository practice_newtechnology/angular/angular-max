import { Component } from '@angular/core';

@Component({
  selector: 'app-root', // Selector should be unique and used as HTML tag in other components
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent { // export - then only accessed by other components
  title = 'AngularMax';
}

/**
 * 1. Decorator always start at @ - it added special features compared to normal class
 * 2. @Component => it parse this file and compiles to javascript 
 * 3. To create component - ng g c {component name}
 */

