import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';

import { AppComponent } from './app.component';
import { AngularComponent } from './angular/angular.component'; // extension(.ts) was added by webpack which bundles the project automatically
import { BasicsComponent } from './angular/basics/basics.component';
import { SelectByElementComponent } from './angular/basics/selector/select-by-element/select-by-element.component';
import { SelectByAttributeComponent } from './angular/basics/selector/select-by-attribute/select-by-attribute.component';
import { SelectByClassComponent } from './angular/basics/selector/select-by-class/select-by-class.component';
import { InlineTemplateComponent } from './angular/basics/template/inline-template/inline-template.component';
import { ExternalFileTemplateComponent } from './angular/basics/template/external-file-template/external-file-template.component';
import { InlineStyleComponent } from './angular/basics/style/inline-style/inline-style.component';
import { ExternalFileStyleComponent } from './angular/basics/style/external-file-style/external-file-style.component';
import { DataBindingComponent } from './angular/data-binding/data-binding.component';
import { DirectivesComponent } from './angular/directives/directives.component';
import { BtwCompComponent } from './angular/data-binding/btw-comp/btw-comp.component';
import { ParentComponent } from './angular/data-binding/btw-comp/input-output/parent/parent.component';
import { ChildComponent } from './angular/data-binding/btw-comp/input-output/child/child.component';
import { LifeCycleHookComponent } from './angular/life-cycle-hook/life-cycle-hook.component';
import { ChildLifecycleComponent } from './angular/life-cycle-hook/child-lifecycle/child-lifecycle.component';
import { CustomAttributeDirective } from './angular/directives/custom-atrribute/custom-attribute.directive';
import { CustomAttributeDatabindingDirective } from './angular/directives/custom-attribute-databinding/custom-attribute-databinding.directive';
import { CustomIfDirective } from './angular/directives/custom-if/custom-if.directive';
import { FirstComponent } from './angular/data-binding/btw-comp/services/first/first.component';
import { SecondComponent } from './angular/data-binding/btw-comp/services/second/second.component';
import { FormComponent } from './angular/form/form.component';
import { PipesComponent } from './angular/pipes/pipes.component';
import { ShortenPipe } from './angular/pipes/shorten-pipe/shorten.pipe';
import { PureFilterPipe } from './angular/pipes/pure-filter-pipe/pure-filter.pipe';
import { ImpureFilterPipe } from './angular/pipes/impure-filter-pipe/impure-filter.pipe';
import { HttpComponent } from './angular/http/http.component';
import { AuthInterceptor } from './angular/http/auth.interceptor';
import { LoggingInterceptor } from './angular/http/logging.interceptor';

@NgModule({  // Angular application split up into multiple modules
  declarations: [  // Declaration - Register the new components in the Modules
    AppComponent,
    AngularComponent,
    BasicsComponent,
    SelectByElementComponent,
    SelectByAttributeComponent,
    SelectByClassComponent,
    InlineTemplateComponent,
    ExternalFileTemplateComponent,
    InlineStyleComponent,
    ExternalFileStyleComponent,
    DataBindingComponent,
    DirectivesComponent,
    BtwCompComponent,
    ParentComponent,
    ChildComponent,
    LifeCycleHookComponent,
    ChildLifecycleComponent,
    CustomAttributeDirective,
    CustomAttributeDatabindingDirective,
    CustomIfDirective,
    FirstComponent,
    SecondComponent,
    FormComponent,
    PipesComponent,
    ShortenPipe,
    PureFilterPipe,
    ImpureFilterPipe,
    HttpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,  // Imported because of ngModel directive used
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [  // Service was used by this module
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoggingInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent] // Bootstrap components was known at the point of application starts
})
export class AppModule { }
