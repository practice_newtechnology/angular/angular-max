# AngularMax

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Development - 850 x Height

## Learning Angular tutorial offered by Max

Single Component
1. Basics
2. Databinding
3. Directives(Built-In and Custom)

More than one component
1. Databinding(input/output)
2. ng-content
3. Lifecycle hooks
4. Services

Concepts
1. Http

Need to learn and implement again
1. Services
2. Router
3. Observables(Need to learn from RxJS Project)